const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const Webpack = require('webpack');

module.exports = {
  // 1 - entry point
  entry: [
    'webpack-dev-server/client?http://0.0.0.0:8081', // WebpackDevServer host and port
    'webpack/hot/only-dev-server', // "only" prevents reload on syntax errors
    './src/index.js',
  ],
  // 2- output
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app/[name].js',
  },
  devServer: {
    contentBase: './dist',
    // added to resolve the issue: https://stackoverflow.com/questions/32098076/react-router-cannot-get-except-for-root-url
    historyApiFallback: true,
  },
  resolve: {
    extensions: ['.js', '.json', '.jsx'],
  },
  // 3- loaders
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
          },
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['eslint-loader'],
      },
      // "css" loader resolves paths in CSS and adds assets as dependencies.
      // "style" loader turns CSS into JS modules that inject <style> tags.
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: true,
              localIdentName: '[name]__[local]--[hash:base64:5]',
            },
          },
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/',
          },
        }],
      },
    ],
  },
  // 4- plugins
  plugins: [
    new HtmlWebPackPlugin({
      template: './src/index.html',
      filename: './index.html',
    }),
    new Webpack.HotModuleReplacementPlugin(),
  ],
};
