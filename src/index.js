import React from 'react';
import ReactDOM from 'react-dom';

import App from './app';

require('./fonts/Roboto.css');

ReactDOM.render(
  <App />,
  document.getElementById('root'),
);
