import React from 'react';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

class LoginPage extends React.Component {
  state = {
    username: '',
    password: '',
  }

  handleInputChange = ({ target: { name, value } }) => {
    this.setState({
      [name]: value,
    });
  }

  render = () => (
    <div>
      <MuiThemeProvider>
        <div>
          <AppBar
            title="Login"
          />
          <TextField
            name="username"
            hintText="Enter your Username"
            floatingLabelText="Username"
            onChange={this.handleInputChange}
          />
          <br />
          <TextField
            name="password"
            type="password"
            hintText="Enter your Password"
            floatingLabelText="Password"
            onChange={this.handleInputChange}
          />
          <br />
          <RaisedButton label="Login" primary onClick={() => this.props.onLogin(this.props.history, this.state.username, this.state.password)} />
        </div>
      </MuiThemeProvider>
    </div>
  );
}

LoginPage.propTypes = {
  onLogin: PropTypes.func.isRequired,
  // eslint-disable-next-line
  history: PropTypes.object.isRequired,
};

export default LoginPage;
