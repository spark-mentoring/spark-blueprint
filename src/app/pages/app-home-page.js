import React from 'react';
import PropTypes from 'prop-types';


const AppHomePage = ({ onLogout }) => (
  <div>
    App Home Page
    <br /><br />
    <button
      onClick={onLogout}
    >
      Logout
    </button>
  </div>
);

AppHomePage.propTypes = {
  onLogout: PropTypes.func.isRequired,
};

export default AppHomePage;
