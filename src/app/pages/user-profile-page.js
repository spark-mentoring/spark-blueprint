import React from 'react';
import PropTypes from 'prop-types';

const UserProfilePage = ({ match }) => (
  <div>
    User Profile for user: {match.params.userId}
  </div>
);

UserProfilePage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.node,
    }).isRequired,
  }).isRequired,
};

export default UserProfilePage;
