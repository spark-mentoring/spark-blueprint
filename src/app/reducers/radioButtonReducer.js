import {
  GET_RADIO_BUTTON_SELLECTION,
  SET_RADIO_BUTTON_SELLECTION,
} from '../actions/types';

const initialState = { radionButtons:{

  } };

const radioButtonReducer = (state = initialState, action) => {
  if (action.type === GET_RADIO_BUTTON_SELLECTION) {
    return Object.assign({}, state, {
      pending: false,
    });
  }

  if (action.type === SET_LOGGED_USER) {
    return Object.assign({}, state, {
      pending: false,
      logged: action.logged,
    });
  }

  return state;
};

export default radioButtonReducer;
