import {
  GET_LOGGED_USER,
  SET_LOGGED_USER,
} from './types';

export const getLoggedUser = () => (
  {
    type: GET_LOGGED_USER,
  }
);

export const login = () => (
  {
    type: SET_LOGGED_USER,
    logged: true,
  }
);

export const logout = () => (
  {
    type: SET_LOGGED_USER,
    logged: false,
  }
);
