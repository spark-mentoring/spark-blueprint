import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import PrimaryHeader from '../components/primary-header';
import AppHomePage from '../containers/app-home-container';
// Sub Layouts
import UserSubLayout from './user-sub-layout';

const PrimaryLayout = ({ computedMatch }) => (
  <div className="primary-layout">
    <PrimaryHeader />
    <main>
      <Switch>
        <Route path={`${computedMatch.path}`} exact component={AppHomePage} />
        <Route path={`${computedMatch.path}/users`} component={UserSubLayout} />
        <Redirect to={`${computedMatch.url}`} />
      </Switch>
    </main>
  </div>
);

PrimaryLayout.propTypes = {
  computedMatch: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.node,
    }).isRequired,
  }).isRequired,
};

export default PrimaryLayout;
