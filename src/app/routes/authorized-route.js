import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class AuthorizedRoute extends React.Component {
  componentWillMount() {
  }

  render() {
    const {
      component: Component,
      pending,
      logged,
      ...rest
    } = this.props;
    return (
      <Route
        {...rest}
        render={() => {
          if (pending) return <div>Loading...</div>;
          return logged
            ? <Component {...this.props} />
            : <Redirect to="/auth/login" />;
        }}
      />
    );
  }
}

AuthorizedRoute.propTypes = {
  component: PropTypes.func.isRequired,
  pending: PropTypes.bool,
  logged: PropTypes.bool,
};

AuthorizedRoute.defaultProps = {
  pending: false,
  logged: false,
};

const mapStateToProps = state => ({
  pending: state.loggedUserState.pending,
  logged: state.loggedUserState.logged,
});

export default connect(
  mapStateToProps,
  null,
)(AuthorizedRoute);
