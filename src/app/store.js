import { createStore, combineReducers } from 'redux';
import loggedUserReducer from './reducers/loggedUserReducer';

const reducers = combineReducers({
  loggedUserState: loggedUserReducer,
});

// eslint-disable-next-line
const store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;
