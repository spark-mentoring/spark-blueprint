import { connect } from 'react-redux';
import { logout } from '../actions';
import AppHomePage from '../pages/app-home-page';

const mapDispatchToProps = dispatch => (
  {
    onLogout: () => {
      dispatch(logout());
    },
  }
);

export default connect(
  null,
  mapDispatchToProps,
)(AppHomePage);
