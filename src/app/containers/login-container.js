import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { login } from '../actions';
import LoginPage from '../pages/login-page';

const mapDispatchToProps = dispatch => (
  {
    onLogin: (history, username, password) => {
      console.info(username, password);
      dispatch(login());
      history.push('/app');
    },
  }
);

export default withRouter(connect(null, mapDispatchToProps)(LoginPage));
