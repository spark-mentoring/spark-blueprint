import React from 'react';
import styled from 'styled-components';

import StyledNav from './styled-components/styled-nav';
import StyledNavLink from './styled-components/styled-nav-link';

const Header = styled.header`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;


const PrimaryHeader = () => (
  <Header className="primary-header">
    <h1>Welcome to our app!</h1>
    <StyledNav>
      <StyledNavLink to="/app" exact activeClassName="active">Home</StyledNavLink>
      <StyledNavLink to="/app/users" activeClassName="active">Users</StyledNavLink>
    </StyledNav>
  </Header>
);

export default PrimaryHeader;
