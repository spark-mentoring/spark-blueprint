import React from 'react';
import styled from 'styled-components'

const StyledInput= styled.input`
  visibility:hidden;
  position: absolute;
`;

const LabelSelected = styled.div`
 background: ${props => props.dark ? 'white' : '#237da0' };
 border-radius: 5px;
 color: ${props => props.dark ? '#3986ae' : 'white'};
 padding: 10px 10px;
 display: inline-block;
 `;

const LabelUnSelected = styled.div`
 background: ${props => props.dark ? '#226784' : 'white' };
 border-radius: 5px;
 color: ${props => props.dark ? 'white' : '#226784'};
 padding: 10px 10px;
 display: inline-block;
 `;

class RadioItem extends React.Component {

  render() {
    const { option, dark, handleOptionChange, checked } = this.props
    if (checked(option)) {
      return (<LabelSelected dark={dark}>
        <StyledInput id={option} type="radio" value={option} checked={checked(option)} onChange={handleOptionChange} />
        <label htmlFor={option}>{option}</label>
      </LabelSelected>)
      }else {
        return (<LabelUnSelected dark={dark}>
          <StyledInput id={option} type="radio" value={option} checked={checked(option)} onChange={handleOptionChange} />
          <label htmlFor={option}>{option}</label>
        </LabelUnSelected>)
      }
  }
}

export default RadioItem
