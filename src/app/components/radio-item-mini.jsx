import React from 'react';
import styled from 'styled-components'

const StyledInput= styled.input`
  visibility:hidden;
  position: absolute;
`;

const LabelSelected = styled.div`
 width:  4em;
 font-size: 0.75em;
 padding: 0.25em 1em;
 background: ${props => props.dark ? 'white' : '#237da0' };
 border-radius: 5px;
 color: ${props => props.dark ? '#3986ae' : 'white'};
 display: inline-block;
 `;

const LabelUnSelected = styled.div`
 width:  4em;
 font-size: 0.75em;
 padding: 0.25em 1em;
 background: ${props => props.dark ? '#226784' : '#e2ecf3' };
 border-radius: 5px;
 color: ${props => props.dark ? '#e2edf3' : '#226784'};
 display: inline-block;
 `;

class RadioItemMini extends React.Component {

  render() {
    const { option, dark, handleOptionChange, checked } = this.props
    if (checked(option)) {
      return (<LabelSelected dark={dark}>
        <StyledInput id={option} type="radio" value={option} checked={checked(option)} onChange={handleOptionChange} />
        <label htmlFor={option}>{option}</label>
      </LabelSelected>)
      }else {
        return (<LabelUnSelected dark={dark}>
          <StyledInput id={option} type="radio" value={option} checked={checked(option)} onChange={handleOptionChange} />
          <label htmlFor={option}>{option}</label>
        </LabelUnSelected>)
      }
  }
}

export default RadioItemMini
