import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import StyledNav from './styled-components/styled-nav';
import StyledNavLink from './styled-components/styled-nav-link';

const UserNav = ({ match }) => (
  <StyledNav className="context-nav">
    <StyledNavLink to={`${match.path}`} exact activeClassName="active">Browse</StyledNavLink>
    <StyledNavLink to={`${match.path}/add`} activeClassName="active">Add</StyledNavLink>
  </StyledNav>
);

UserNav.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.node,
    }).isRequired,
  }).isRequired,
};

export default withRouter(UserNav);
