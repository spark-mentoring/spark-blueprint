import styled from 'styled-components';

const StyledNav = styled.nav`
  display: flex;
  flex-direction: row;
`;

export default StyledNav;
