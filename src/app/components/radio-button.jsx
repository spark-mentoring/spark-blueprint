import React from 'react';
import styled from 'styled-components'
import RadioItem from './radio-item'
import RadioItemMini from './radio-item-mini'
import RadioItemSmall from './radio-item-small'

const StyledFieldset = styled.fieldset`
 padding: 5px 5px;
 border: ${props => props.type !== 'mini' ? `1px solid ${props.color}` : 'none'};
 border-radius: 5px;
 background: ${props => props.dark ? '#226784' : 'white'};
 display: inline-block;
 vertical-align: middle;
`;

const StyledFieldSetContainer = styled.div`
 background: ${props => props.dark ? '#207da0' : 'white'};
 padding: 5px 5px;
 display: inline-block;
 `;

class RadioButton extends React.Component {

  constructor (props) {
    super(props);
      this.state = { selectedOption : this.props.options[0] }
  }

  handleOptionChange = (changeEvent) => {
    this.setState({
      selectedOption : changeEvent.target.value
    });
  }

  checked = (option) => {
    return this.state.selectedOption === option
  }

  render() {
    const { options, dark, type, namespace } = this.props
    const borderColor = type === 'small' ? '#c5cfd6' : '#237da0'

    const radioButtons = options.map((option) => {
      if (type === 'mini') {
        return (<RadioItemMini key={option} option={option} dark={dark} checked={this.checked} handleOptionChange={this.handleOptionChange}/>)
      }
      if (type === 'small') {
        return (<RadioItemSmall key={option} option={option} dark={dark} checked={this.checked} handleOptionChange={this.handleOptionChange}/>)
      }
      return (<RadioItem key={option} option={option} dark={dark} checked={this.checked} handleOptionChange={this.handleOptionChange}/>)
    })
    return (
       <StyledFieldSetContainer dark={dark} type={type}>
         <StyledFieldset dark={dark} type={type} color={borderColor}>
           {radioButtons}
         </StyledFieldset>
       </StyledFieldSetContainer>
    );
  }
}

export default RadioButton
