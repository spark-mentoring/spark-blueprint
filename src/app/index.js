import React from 'react';
import { hot } from 'react-hot-loader';
import { Provider } from 'react-redux';
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import styled from 'styled-components';

import AuthorizedRoute from './routes/authorized-route';
import store from './store';
import UnauthorizedLayout from './layouts/unauthorized-layout';
import PrimaryLayout from './layouts/primary-layout';

const AppWrapper = styled.div`
  font-family: Roboto;
`;

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <AppWrapper>
        <Switch>
          <Route path="/auth" component={UnauthorizedLayout} />
          <AuthorizedRoute path="/app" component={PrimaryLayout} />
          <Redirect to="/auth" />
        </Switch>
      </AppWrapper>
    </BrowserRouter>
  </Provider>
);

export default hot(module)(App);
